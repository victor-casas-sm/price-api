FROM maven:3.9.6-eclipse-temurin-21-jammy as builder

COPY .mvn ./.mvn
COPY pom.xml .
COPY src ./src
RUN mvn package -DskipTests

RUN cp target/*.jar /opt/priceapi.jar 
 
FROM eclipse-temurin:21-jre-jammy

COPY --from=builder /opt/priceapi.jar .
EXPOSE 8080
CMD ["java", "-jar", "priceapi.jar"]